# This script install/activate a virtual environment 
# && installs py packages inside the virtual environment

python3 -m venv .venv

source .venv/bin/activate

pip3 install -r requirements.txt 

