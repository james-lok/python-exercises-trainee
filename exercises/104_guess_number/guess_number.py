# Gueesing the magic number game!
# equate objects and generate booleans

# As a client, I want to be able to guess a number and know if i got it correct or not, so that I can know if I won or not.


#  create a function called guess_number that returns a string


# the function should take two numbers


# if the second number is larger than the first. Output 'this number is too large'
# if the second number is smaller than the first. Output 'this number is too small'
# if the two numbers are the same, it should output 'this number is the same'


def guess_number(n, m):
    if(n<m):
        return "this number is too large"
    elif(n>m):
        return "this number is too small"
    else:
        return "this number is the same"

