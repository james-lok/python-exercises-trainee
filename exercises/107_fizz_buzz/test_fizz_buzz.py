# pylint: skip-file
from fizzbuzz_functions import *

def test_answer_1():
    assert check_factor3(3) == True

def test_answer_2():
    assert check_factor5(5) == True

def test_answer_3():
    assert check_factor5(8) == False

def test_answer_4():
    assert check_factor3_5(5) == False
