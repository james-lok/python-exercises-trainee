# Python Exercises 

This repository contains various python exercises which will help each trainee work on their python skills. 

The exercises start off simple from calling variables to taking user inputs and creating amazing games all with a single python script.

The aim of these tasks is to help each trainee develop their own individual python skills and highlight any areas that they may need to improve. This will also allow your trainer to get a clear picture on how everyone is doing and recognise if any topics require additional clarification.


## Pre- Requisites 

- Python and pip3 Installed
- Ask trainer for jenkins password before you run 'setvar.sh' and use the user name and password as arguments when running scripts.

```
.setvar.sh [username] [password]
```
  
-------------------------------------------------------

## Getting started

### Fork this repository and clone to your local machine.

![Fork-repository](./fork-repository.jpeg)

```bash

# Clone your new repository
$ git clone git@bitbucket.org:<yourname>/project-progress-pipelines.git

# navigate to root of project
$ cd path/to/repo

# Run bashscript to install virtual environment and python packages
$ ./installation.sh

# Activate your virtual environment
$ source .venv/bin/activate

```

## Start Python exercises

You will see all the tasks in the 'exercise' folder and you start from the first folder called *101_strings* and work your way down.

When in the path of each folder, you will get to test your code and check if it passes or needs work. 

```bash 
# To test your code 
$ pytest

# To enforce a coding standard and look for code smells
$ pylint <file.name>

# To test your code for errors and code smells at the same time
$ pytest --pylint

# To have feedback in a HTML format
$ pytest --pylint --html=report.html
```
